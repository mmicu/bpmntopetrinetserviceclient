package test;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import main.Main;

import org.xml.sax.SAXException;

import data.structure.HTTPResponse;
import parser.ParserResponse;
import utils.Utils;

public class TestHTTP
{
    public static void main(String[] args)
    {
        /*
        POST /BPMNtoPetriNetService/services/BPMNtoPetriNetService.BPMNtoPetriNetServiceHttpSoap11Endpoint/ HTTP/1.1
        Content-Type: text/xml
        Content-Encoding: UTF-8
        SOAPAction: urn:checkDeadlock
        Host: localhost:8080
        Content-Length: 3817
        
        <s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'>
          <s11:Body>
            <ns1:checkDeadlock xmlns:ns1='http://service'>
              <ns1:bpmnEncoded>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxicG1uMjpkZWZpbml0aW9ucyB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4bWxuczpicG1uMj0iaHR0cDovL3d3dy5vbWcub3JnL3NwZWMvQlBNTi8yMDEwMDUyNC9NT0RFTCIgeG1sbnM6YnBtbmRpPSJodHRwOi8vd3d3Lm9tZy5vcmcvc3BlYy9CUE1OLzIwMTAwNTI0L0RJIiB4bWxuczpkYz0iaHR0cDovL3d3dy5vbWcub3JnL3NwZWMvREQvMjAxMDA1MjQvREMiIHhtbG5zOmRpPSJodHRwOi8vd3d3Lm9tZy5vcmcvc3BlYy9ERC8yMDEwMDUyNC9ESSIgaWQ9IkRlZmluaXRpb25zXzEiIHRhcmdldE5hbWVzcGFjZT0iaHR0cDovL29yZy5lY2xpcHNlLmJwbW4yL2RlZmF1bHQvcHJvY2VzcyI+DQogIDxicG1uMjpwcm9jZXNzIGlkPSJwcm9jZXNzXzEiIG5hbWU9IkRlZmF1bHQgUHJvY2VzcyI+DQogICAgPGJwbW4yOnN0YXJ0RXZlbnQgaWQ9IlN0YXJ0RXZlbnRfMSIgbmFtZT0iU3RhcnQgZXZlbnQgMSI+DQogICAgICA8YnBtbjI6b3V0Z29pbmc+U2VxdWVuY2VGbG93XzI8L2JwbW4yOm91dGdvaW5nPg0KICAgIDwvYnBtbjI6c3RhcnRFdmVudD4NCiAgICA8YnBtbjI6dGFzayBpZD0iVGFza18xIiBuYW1lPSJUYXNrIDEiPg0KICAgICAgPGJwbW4yOmluY29taW5nPlNlcXVlbmNlRmxvd18yPC9icG1uMjppbmNvbWluZz4NCiAgICAgIDxicG1uMjpvdXRnb2luZz5TZXF1ZW5jZUZsb3dfMzwvYnBtbjI6b3V0Z29pbmc+DQogICAgPC9icG1uMjp0YXNrPg0KICAgIDxicG1uMjpzZXF1ZW5jZUZsb3cgaWQ9IlNlcXVlbmNlRmxvd18yIiBzb3VyY2VSZWY9IlN0YXJ0RXZlbnRfMSIgdGFyZ2V0UmVmPSJUYXNrXzEiLz4NCiAgICA8YnBtbjI6c2VxdWVuY2VGbG93IGlkPSJTZXF1ZW5jZUZsb3dfMyIgc291cmNlUmVmPSJUYXNrXzEiIHRhcmdldFJlZj0iRW5kRXZlbnRfMiIvPg0KICAgIDxicG1uMjplbmRFdmVudCBpZD0iRW5kRXZlbnRfMiIgbmFtZT0iRW5kIEV2ZW50IDIiPg0KICAgICAgPGJwbW4yOmluY29taW5nPlNlcXVlbmNlRmxvd18zPC9icG1uMjppbmNvbWluZz4NCiAgICA8L2JwbW4yOmVuZEV2ZW50Pg0KICA8L2JwbW4yOnByb2Nlc3M+DQogIDxicG1uZGk6QlBNTkRpYWdyYW0gaWQ9IkJQTU5EaWFncmFtXzEiIG5hbWU9IkRlZmF1bHQgUHJvY2VzcyBEaWFncmFtIj4NCiAgICA8YnBtbmRpOkJQTU5QbGFuZSBpZD0iQlBNTlBsYW5lXzEiIGJwbW5FbGVtZW50PSJwcm9jZXNzXzEiPg0KICAgICAgPGJwbW5kaTpCUE1OU2hhcGUgaWQ9IkJQTU5TaGFwZV8xIiBicG1uRWxlbWVudD0iU3RhcnRFdmVudF8xIj4NCiAgICAgICAgPGRjOkJvdW5kcyBoZWlnaHQ9IjM2LjAiIHdpZHRoPSIzNi4wIiB4PSIxMDAuMCIgeT0iMjMwLjAiLz4NCiAgICAgIDwvYnBtbmRpOkJQTU5TaGFwZT4NCiAgICAgIDxicG1uZGk6QlBNTlNoYXBlIGlkPSJCUE1OU2hhcGVfVGFza18xIiBicG1uRWxlbWVudD0iVGFza18xIj4NCiAgICAgICAgPGRjOkJvdW5kcyBoZWlnaHQ9IjUwLjAiIHdpZHRoPSIxMTAuMCIgeD0iMjIwLjAiIHk9IjIyMy4wIi8+DQogICAgICA8L2JwbW5kaTpCUE1OU2hhcGU+DQogICAgICA8YnBtbmRpOkJQTU5TaGFwZSBpZD0iQlBNTlNoYXBlX0VuZEV2ZW50XzEiIGJwbW5FbGVtZW50PSJFbmRFdmVudF8yIj4NCiAgICAgICAgPGRjOkJvdW5kcyBoZWlnaHQ9IjM2LjAiIHdpZHRoPSIzNi4wIiB4PSI0MDAuMCIgeT0iMjMwLjAiLz4NCiAgICAgIDwvYnBtbmRpOkJQTU5TaGFwZT4NCiAgICAgIDxicG1uZGk6QlBNTkVkZ2UgaWQ9IkJQTU5FZGdlX1NlcXVlbmNlRmxvd18yIiBicG1uRWxlbWVudD0iU2VxdWVuY2VGbG93XzIiIHNvdXJjZUVsZW1lbnQ9IkJQTU5TaGFwZV8xIiB0YXJnZXRFbGVtZW50PSJCUE1OU2hhcGVfVGFza18xIj4NCiAgICAgICAgPGRpOndheXBvaW50IHhzaTp0eXBlPSJkYzpQb2ludCIgeD0iMTM2LjAiIHk9IjI0OC4wIi8+DQogICAgICAgIDxkaTp3YXlwb2ludCB4c2k6dHlwZT0iZGM6UG9pbnQiIHg9IjIwMC4wIiB5PSIyNDguMCIvPg0KICAgICAgICA8ZGk6d2F5cG9pbnQgeHNpOnR5cGU9ImRjOlBvaW50IiB4PSIyMDAuMCIgeT0iMjQ4LjAiLz4NCiAgICAgICAgPGRpOndheXBvaW50IHhzaTp0eXBlPSJkYzpQb2ludCIgeD0iMjIwLjAiIHk9IjI0OC4wIi8+DQogICAgICA8L2JwbW5kaTpCUE1ORWRnZT4NCiAgICAgIDxicG1uZGk6QlBNTkVkZ2UgaWQ9IkJQTU5FZGdlX1NlcXVlbmNlRmxvd18zIiBicG1uRWxlbWVudD0iU2VxdWVuY2VGbG93XzMiIHNvdXJjZUVsZW1lbnQ9IkJQTU5TaGFwZV9UYXNrXzEiIHRhcmdldEVsZW1lbnQ9IkJQTU5TaGFwZV9FbmRFdmVudF8xIj4NCiAgICAgICAgPGRpOndheXBvaW50IHhzaTp0eXBlPSJkYzpQb2ludCIgeD0iMzMwLjAiIHk9IjI0OC4wIi8+DQogICAgICAgIDxkaTp3YXlwb2ludCB4c2k6dHlwZT0iZGM6UG9pbnQiIHg9IjM4MC4wIiB5PSIyNDguMCIvPg0KICAgICAgICA8ZGk6d2F5cG9pbnQgeHNpOnR5cGU9ImRjOlBvaW50IiB4PSIzODAuMCIgeT0iMjQ4LjAiLz4NCiAgICAgICAgPGRpOndheXBvaW50IHhzaTp0eXBlPSJkYzpQb2ludCIgeD0iNDAwLjAiIHk9IjI0OC4wIi8+DQogICAgICA8L2JwbW5kaTpCUE1ORWRnZT4NCiAgICA8L2JwbW5kaTpCUE1OUGxhbmU+DQogIDwvYnBtbmRpOkJQTU5EaWFncmFtPg0KPC9icG1uMjpkZWZpbml0aW9ucz4=</ns1:bpmnEncoded>
            </ns1:checkDeadlock>
          </s11:Body>
        </s11:Envelope>
         */
        String data = "<s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'>" + 
          "<s11:Body>" + 
            "<ns1:checkDeadlock xmlns:ns1='http://service'>" + 
              "<ns1:bpmnEncoded>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxicG1uMjpkZWZpbml0aW9ucyB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4bWxuczpicG1uMj0iaHR0cDovL3d3dy5vbWcub3JnL3NwZWMvQlBNTi8yMDEwMDUyNC9NT0RFTCIgeG1sbnM6YnBtbmRpPSJodHRwOi8vd3d3Lm9tZy5vcmcvc3BlYy9CUE1OLzIwMTAwNTI0L0RJIiB4bWxuczpkYz0iaHR0cDovL3d3dy5vbWcub3JnL3NwZWMvREQvMjAxMDA1MjQvREMiIHhtbG5zOmRpPSJodHRwOi8vd3d3Lm9tZy5vcmcvc3BlYy9ERC8yMDEwMDUyNC9ESSIgaWQ9IkRlZmluaXRpb25zXzEiIHRhcmdldE5hbWVzcGFjZT0iaHR0cDovL29yZy5lY2xpcHNlLmJwbW4yL2RlZmF1bHQvcHJvY2VzcyI+DQogIDxicG1uMjpwcm9jZXNzIGlkPSJwcm9jZXNzXzEiIG5hbWU9IkRlZmF1bHQgUHJvY2VzcyI+DQogICAgPGJwbW4yOnN0YXJ0RXZlbnQgaWQ9IlN0YXJ0RXZlbnRfMSIgbmFtZT0iU3RhcnQgZXZlbnQgMSI+DQogICAgICA8YnBtbjI6b3V0Z29pbmc+U2VxdWVuY2VGbG93XzI8L2JwbW4yOm91dGdvaW5nPg0KICAgIDwvYnBtbjI6c3RhcnRFdmVudD4NCiAgICA8YnBtbjI6dGFzayBpZD0iVGFza18xIiBuYW1lPSJUYXNrIDEiPg0KICAgICAgPGJwbW4yOmluY29taW5nPlNlcXVlbmNlRmxvd18yPC9icG1uMjppbmNvbWluZz4NCiAgICAgIDxicG1uMjpvdXRnb2luZz5TZXF1ZW5jZUZsb3dfMzwvYnBtbjI6b3V0Z29pbmc+DQogICAgPC9icG1uMjp0YXNrPg0KICAgIDxicG1uMjpzZXF1ZW5jZUZsb3cgaWQ9IlNlcXVlbmNlRmxvd18yIiBzb3VyY2VSZWY9IlN0YXJ0RXZlbnRfMSIgdGFyZ2V0UmVmPSJUYXNrXzEiLz4NCiAgICA8YnBtbjI6c2VxdWVuY2VGbG93IGlkPSJTZXF1ZW5jZUZsb3dfMyIgc291cmNlUmVmPSJUYXNrXzEiIHRhcmdldFJlZj0iRW5kRXZlbnRfMiIvPg0KICAgIDxicG1uMjplbmRFdmVudCBpZD0iRW5kRXZlbnRfMiIgbmFtZT0iRW5kIEV2ZW50IDIiPg0KICAgICAgPGJwbW4yOmluY29taW5nPlNlcXVlbmNlRmxvd18zPC9icG1uMjppbmNvbWluZz4NCiAgICA8L2JwbW4yOmVuZEV2ZW50Pg0KICA8L2JwbW4yOnByb2Nlc3M+DQogIDxicG1uZGk6QlBNTkRpYWdyYW0gaWQ9IkJQTU5EaWFncmFtXzEiIG5hbWU9IkRlZmF1bHQgUHJvY2VzcyBEaWFncmFtIj4NCiAgICA8YnBtbmRpOkJQTU5QbGFuZSBpZD0iQlBNTlBsYW5lXzEiIGJwbW5FbGVtZW50PSJwcm9jZXNzXzEiPg0KICAgICAgPGJwbW5kaTpCUE1OU2hhcGUgaWQ9IkJQTU5TaGFwZV8xIiBicG1uRWxlbWVudD0iU3RhcnRFdmVudF8xIj4NCiAgICAgICAgPGRjOkJvdW5kcyBoZWlnaHQ9IjM2LjAiIHdpZHRoPSIzNi4wIiB4PSIxMDAuMCIgeT0iMjMwLjAiLz4NCiAgICAgIDwvYnBtbmRpOkJQTU5TaGFwZT4NCiAgICAgIDxicG1uZGk6QlBNTlNoYXBlIGlkPSJCUE1OU2hhcGVfVGFza18xIiBicG1uRWxlbWVudD0iVGFza18xIj4NCiAgICAgICAgPGRjOkJvdW5kcyBoZWlnaHQ9IjUwLjAiIHdpZHRoPSIxMTAuMCIgeD0iMjIwLjAiIHk9IjIyMy4wIi8+DQogICAgICA8L2JwbW5kaTpCUE1OU2hhcGU+DQogICAgICA8YnBtbmRpOkJQTU5TaGFwZSBpZD0iQlBNTlNoYXBlX0VuZEV2ZW50XzEiIGJwbW5FbGVtZW50PSJFbmRFdmVudF8yIj4NCiAgICAgICAgPGRjOkJvdW5kcyBoZWlnaHQ9IjM2LjAiIHdpZHRoPSIzNi4wIiB4PSI0MDAuMCIgeT0iMjMwLjAiLz4NCiAgICAgIDwvYnBtbmRpOkJQTU5TaGFwZT4NCiAgICAgIDxicG1uZGk6QlBNTkVkZ2UgaWQ9IkJQTU5FZGdlX1NlcXVlbmNlRmxvd18yIiBicG1uRWxlbWVudD0iU2VxdWVuY2VGbG93XzIiIHNvdXJjZUVsZW1lbnQ9IkJQTU5TaGFwZV8xIiB0YXJnZXRFbGVtZW50PSJCUE1OU2hhcGVfVGFza18xIj4NCiAgICAgICAgPGRpOndheXBvaW50IHhzaTp0eXBlPSJkYzpQb2ludCIgeD0iMTM2LjAiIHk9IjI0OC4wIi8+DQogICAgICAgIDxkaTp3YXlwb2ludCB4c2k6dHlwZT0iZGM6UG9pbnQiIHg9IjIwMC4wIiB5PSIyNDguMCIvPg0KICAgICAgICA8ZGk6d2F5cG9pbnQgeHNpOnR5cGU9ImRjOlBvaW50IiB4PSIyMDAuMCIgeT0iMjQ4LjAiLz4NCiAgICAgICAgPGRpOndheXBvaW50IHhzaTp0eXBlPSJkYzpQb2ludCIgeD0iMjIwLjAiIHk9IjI0OC4wIi8+DQogICAgICA8L2JwbW5kaTpCUE1ORWRnZT4NCiAgICAgIDxicG1uZGk6QlBNTkVkZ2UgaWQ9IkJQTU5FZGdlX1NlcXVlbmNlRmxvd18zIiBicG1uRWxlbWVudD0iU2VxdWVuY2VGbG93XzMiIHNvdXJjZUVsZW1lbnQ9IkJQTU5TaGFwZV9UYXNrXzEiIHRhcmdldEVsZW1lbnQ9IkJQTU5TaGFwZV9FbmRFdmVudF8xIj4NCiAgICAgICAgPGRpOndheXBvaW50IHhzaTp0eXBlPSJkYzpQb2ludCIgeD0iMzMwLjAiIHk9IjI0OC4wIi8+DQogICAgICAgIDxkaTp3YXlwb2ludCB4c2k6dHlwZT0iZGM6UG9pbnQiIHg9IjM4MC4wIiB5PSIyNDguMCIvPg0KICAgICAgICA8ZGk6d2F5cG9pbnQgeHNpOnR5cGU9ImRjOlBvaW50IiB4PSIzODAuMCIgeT0iMjQ4LjAiLz4NCiAgICAgICAgPGRpOndheXBvaW50IHhzaTp0eXBlPSJkYzpQb2ludCIgeD0iNDAwLjAiIHk9IjI0OC4wIi8+DQogICAgICA8L2JwbW5kaTpCUE1ORWRnZT4NCiAgICA8L2JwbW5kaTpCUE1OUGxhbmU+DQogIDwvYnBtbmRpOkJQTU5EaWFncmFtPg0KPC9icG1uMjpkZWZpbml0aW9ucz4=</ns1:bpmnEncoded>" +
            "</ns1:checkDeadlock>" + 
          "</s11:Body>" + 
        "</s11:Envelope>";
        
        HTTPResponse HP = Utils.HTTPRequest (
            "POST",
            "/BPMNtoPetriNetService/services/BPMNtoPetriNetService.BPMNtoPetriNetServiceHttpSoap11Endpoint/",
            "text/xml",
            "UTF-8",
            "urn:checkDeadlock",
            Main.LOCAL_TESTING ? "http://127.0.0.1:8080" : "http://sputnik.cs.unicam.it",
            data
        );
        
        // Test Response
        int rc;
        
        if ((rc = HP.getResponseCode ()) == 200) {
            try {
                ArrayList<String> res = ParserResponse.parse (HP.getResponse ());
                
                System.out.println ("|res| = " + res.size ());
                for (int k = 0, size = res.size (); k < size; k++)
                    System.out.println ("res (" + k + ") = " + res.get (k));
            } 
            catch (ParserConfigurationException | SAXException | IOException e) {
                System.out.println ("Exception! Message: " + e.getMessage ());
            }
        }
        else
            System.out.println ("Response Code = " + rc);
        
        System.exit (0);
    }
}
