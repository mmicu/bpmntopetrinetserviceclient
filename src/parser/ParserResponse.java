package parser;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ParserResponse
{
    public static ArrayList<String> parse (String xmlResponse) throws ParserConfigurationException, SAXException, IOException
    {
        final ArrayList<String> result = new ArrayList<String> ();
        SAXParserFactory factory = SAXParserFactory.newInstance ();
        SAXParser saxParser = factory.newSAXParser ();
     
        DefaultHandler handler = new DefaultHandler () 
        {
            boolean insideTagReturn;
            
            public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
            {
                if (qName.equals ("ns:return"))
                    insideTagReturn = true;
            }
            
            
            public void characters (char ch[], int start, int length) throws SAXException 
            {
                if (insideTagReturn)
                    result.add (new String (ch, start, length));
            }
            
            
            public void endElement (String uri, String localName, String qName) throws SAXException 
            {
                if (qName.equals ("ns:return")) 
                    insideTagReturn = false;
            }
        };

        saxParser.parse (new InputSource (new StringReader (xmlResponse)), handler);
        
        return result;
    }
}
