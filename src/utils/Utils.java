package utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import data.structure.HTTPResponse;

public class Utils
{
    public static final boolean DEBUG = false;

    
    public static HTTPResponse HTTPRequest (String method, String url, String contentType, String encoding, String soapAction, String host, String data)
    {
        String _url = host + url;
        
        try {
            URL objURL = new URL (_url);
            
            HttpURLConnection con = (HttpURLConnection) objURL.openConnection ();
            
            con.setRequestMethod (method);
            con.setRequestProperty ("Content-Type", contentType);
            con.setRequestProperty ("Content-Encoding", encoding);
            con.setRequestProperty ("SOAPAction", soapAction);
            con.setRequestProperty ("Host", host);
            con.setRequestProperty ("Content-Length", new Integer (data.length ()).toString ());
            
            con.setDoOutput (true);
            DataOutputStream wr = new DataOutputStream (con.getOutputStream ());
            wr.writeBytes (data);
            wr.flush ();
            wr.close ();
            
            BufferedReader in = new BufferedReader (new InputStreamReader (con.getInputStream ()));
            String inputLine;
            StringBuffer response = new StringBuffer ();
     
            while ((inputLine = in.readLine ()) != null)
                response.append (inputLine);
            
            in.close();
     
            if (Utils.DEBUG) {
                System.out.println ("Connection.toString ():        " + con.toString ());
                System.out.println ("Sending 'POST' request to URL: " + url);
                System.out.println ("Response code:                 " + con.getResponseCode ());
                System.out.println ("Response:                      " + response.toString ());
            }
            
            return new HTTPResponse (con.toString (), url, con.getResponseCode (), response.toString ());
        } 
        catch (IOException e) {
            System.out.println ("IOException: " + e.getMessage ());
        }
        
        return null;
    }
    
    
    public static String readFile (String path)
    {
        String content = "";
        
        try (BufferedReader br = new BufferedReader (new FileReader (path))) {
            String sCurrentLine;
 
            while ((sCurrentLine = br.readLine ()) != null)
                content += sCurrentLine;
        }
        catch (IOException e) {
            e.printStackTrace();
        } 
        
        return content;
    }
}
