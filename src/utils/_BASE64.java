package utils;

import org.apache.commons.codec.binary.Base64;

public class _BASE64
{
    public static String encodeBASE64 (String str)
    {
        return new String (Base64.encodeBase64 (str.getBytes ()));
    }
    
    
    public static byte[] decodeBASE64 (byte[] bytesEncoded)
    {
        return Base64.decodeBase64 (bytesEncoded);
    }
    
    
    public static String decodeBASE64 (String bytesEncoded)
    {
        return new String (Base64.decodeBase64 (bytesEncoded.getBytes ()));
    }
}
