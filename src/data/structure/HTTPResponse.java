package data.structure;

public class HTTPResponse
{
    private String request;
    
    private String url;
    
    private int responseCode;
    
    private String response;
    
    
    public HTTPResponse (String request, String url, int responseCode, String response)
    {
        this.request      = request;
        this.url          = url;
        this.responseCode = responseCode;
        this.response     = response;
    }


    public String getRequest ()
    {
        return this.request;
    }


    public void setRequest (String request)
    {
        this.request = request;
    }


    public String getUrl ()
    {
        return this.url;
    }


    public void setUrl (String url)
    {
        this.url = url;
    }


    public int getResponseCode ()
    {
        return this.responseCode;
    }


    public void setResponseCode (int responseCode)
    {
        this.responseCode = responseCode;
    }


    public String getResponse ()
    {
        return this.response;
    }


    public void setResponse (String response)
    {
        this.response = response;
    }


    @Override
    public String toString ()
    {
        return "HTTPResponse [request=" + request + ", url=" + url + ", responseCode=" + responseCode + ", response=" + response + "]";
    }
}
