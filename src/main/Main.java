package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import parser.ParserResponse;
import data.structure.HTTPResponse;
import utils.Utils;
import utils._BASE64;

public class Main
{
    public static boolean LOCAL_TESTING = false;
    
    
    private static void usage ()
    {
        System.out.println ("Usage: java -jar jar/BPMNtoPetriNetServiceClient.jar path/of/the/process.bpmn [ --localhost ]");
    }
    
    
    private static String getHTTPData (String content)
    {
        return "<s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'>" + 
                 "<s11:Body>" + 
                   "<ns1:checkDeadlock xmlns:ns1='http://service'>" + 
                   "<ns1:bpmnEncoded>" + _BASE64.encodeBASE64 (content) + "</ns1:bpmnEncoded>" +
                   "</ns1:checkDeadlock>" + 
                 "</s11:Body>" + 
               "</s11:Envelope>";
    }
    
    public static void main (String[] args)
    {
        int argsLength = 0;
        
        if ((argsLength = args.length) > 0) {
            for (int k = 0; k < argsLength; k++)
                if (args[k].trim ().equals ("--localhost"))
                    Main.LOCAL_TESTING = true;
            
            
            if (new File (args[0]).exists ()) {
                HTTPResponse HP = Utils.HTTPRequest (
                    "POST",
                    "/BPMNtoPetriNetService/services/BPMNtoPetriNetService.BPMNtoPetriNetServiceHttpSoap11Endpoint/",
                    "text/xml",
                    "UTF-8",
                    "urn:checkDeadlock",
                    Main.LOCAL_TESTING ? "http://127.0.0.1:8080" : "http://sputnik.cs.unicam.it",
                    Main.getHTTPData (Utils.readFile (args[0]))
                );
                
                // Test Response
                int rc;
                
                if ((rc = HP.getResponseCode ()) == 200) {
                    try {
                        ArrayList<String> res = ParserResponse.parse (HP.getResponse ());
                        
                        System.out.println ("|res| = " + res.size ());
                        for (int k = 0, size = res.size (); k < size; k++)
                            System.out.println ("res (" + k + ") = " + res.get (k));
                    } 
                    catch (ParserConfigurationException | SAXException | IOException e) {
                        System.out.println ("Exception! Message: " + e.getMessage ());
                    }
                }
                else
                    System.out.println ("Response Code = " + rc);
            }
            else {
                System.out.println ("Il file \"" + args[0] + "\" non esiste!");
                System.exit (-1);
            }
        }
        else
            Main.usage ();
        
        System.exit (0);
    }
}
