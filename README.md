# BPMNtoPetriNetServiceClient
___


Installation
---
```sh
ant compile jar
```


Usage
-----
```sh
java -jar build/jar/BPMNtoPetriNetServiceClient.jar path/of/the/process.bpmn
```


Example
-----
If you use:
```sh
java -jar build/jar/BPMNtoPetriNetServiceClient.jar BPMNModels/processes/process_3.bpmn
```
you'll get:
```sh
|res| = 6
res (0) = OK
res (1) = NO , the net is deadlock-free
res (2) = No trace
res (3) = 104
res (4) = 54
res (5) = 46
```
where:

* **res(0)**, indicates the result of the HTTP request;
* **res(1)**, indicates the message of the possible presence of deadlocks;
* **res(2)**, indicates the trace of deadlock (if it's present);
* **res(3)**, indicates the milliseconds for the calculation of the Petri net;
* **res(4)**, indicates the total number of places of the unfolding;
* **res(5)**, indicates the total number of transitions of the unfolding.
